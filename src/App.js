import React , {Component} from 'react';
import './App.css';
import Maps from './Maps.js';


class App extends Component{
  constructor(props){
    super(props);
    this.state={
      inputValue:""
    }
    this.onInputChange=this.onInputChange.bind(this);
  }

  onInputChange=(e)=>{
   
    this.setState({inputValue:e.target.value});
    
  }

  render(){
  return (
    <div className="App">
      <h1>Trouver l'emplacement d'un objet</h1>
        <label>id de l'objet recherché : </label> <input onChange={this.onInputChange}></input>
         <Maps obj={this.state.inputValue}/>
        </div>
  );
}
}

export default App;
